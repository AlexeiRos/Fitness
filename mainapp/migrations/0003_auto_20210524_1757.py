# Generated by Django 3.1.5 on 2021-05-24 14:57

import datetime
from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('mainapp', '0002_information'),
    ]

    operations = [
        migrations.AlterField(
            model_name='information',
            name='end',
            field=models.DateTimeField(default=datetime.date.today, verbose_name='Окончание активности'),
        ),
        migrations.AlterField(
            model_name='information',
            name='start',
            field=models.DateTimeField(default=datetime.date.today, verbose_name='Начала активности'),
        ),
    ]
