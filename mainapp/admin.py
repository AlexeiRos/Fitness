from django.contrib import admin
from .models import Category, Information, Customer

admin.site.register(Category)
admin.site.register(Information)
admin.site.register(Customer)