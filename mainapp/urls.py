from django.urls import path, include
from rest_framework import routers
from .api import *


from .views import (
    fitness,
)

router = routers.DefaultRouter()
router.register(r'Category', CategoryViewSet)
router.register(r'Information', InformationViewSet)

urlpatterns = [
    path('', fitness, name='base'),
    path('api/', include(router.urls)),
    path('accounts/', include('allauth.urls')),
]